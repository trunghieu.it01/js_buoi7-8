var numberArray = [];
function themSo() {
  var inputNumber = document.querySelector("#txt-number");
  if (inputNumber.value == "") {
    return;
  }
  var number = inputNumber.value * 1;
  numberArray.push(number);

  inputNumber.value = "";
  console.log("numberArray: ", numberArray);
  document.getElementById(
    "khaiBaoMang"
  ).innerHTML = `<p>Mảng hiện tại: ${numberArray}</p>`;
  return numberArray;
}

function tinhTongSoDuong() {
  var tongSoDuong = 0;
  soLuongSoDuong = 0;
  for (var i = 0; i < numberArray.length; i++) {
    if (numberArray[i] > 0) {
      tongSoDuong += numberArray[i];
      soLuongSoDuong++;
    }
  }
  document.querySelector(
    ".resultEx1"
  ).innerHTML = `<p>Tổng số dương: ${tongSoDuong}</p>`;
}
function demSoDuong() {
  var soLuongSoDuong = 0;
  for (var i = 0; i < numberArray.length; i++) {
    if (numberArray[i] > 0) {
      soLuongSoDuong++;
    }
  }
  document.querySelector(
    ".resultEx2"
  ).innerHTML = `<p>Số lượng số dương: ${soLuongSoDuong}</p>`;
}
function timSoNhoNhat() {
  var soNhoNhat = numberArray[0];
  for (var i = 1; i < numberArray.length; i++) {
    if (soNhoNhat > numberArray[i]) {
      soNhoNhat = numberArray[i];
    }
  }
  document.querySelector(
    ".resultEx3"
  ).innerHTML = `<p>Số nhỏ nhất trong mảng: ${soNhoNhat}</p>`;
}
var mangSoDuong = [];
function timSoDuongNhoNhat() {
  var result;
  for (var i = 0; i < numberArray.length; i++) {
    if (numberArray[i] > 0) {
      mangSoDuong.push(numberArray[i]);
    }
  }
  if (mangSoDuong.length == 0) {
    result = `<p>Mảng không có số dương</p>`;
  } else {
    var soDuongNhoNhat = mangSoDuong[0];
    for (var i = 0; i < mangSoDuong.length; i++) {
      if (soDuongNhoNhat > mangSoDuong[i]) {
        soDuongNhoNhat = mangSoDuong[i];
      }
      result = `<p> ${soDuongNhoNhat} </p>`;
    }
  }
  document.querySelector(".resultEx4").innerHTML = result;
}

var mangSoChan = [];
function timSoChanCuoiCung() {
  for (var i = 0; i < numberArray.length; i++) {
    var soChanCuoiCung;
    if (numberArray[i] % 2 == 0) {
      mangSoChan.push(numberArray[i]);
      soChanCuoiCung = mangSoChan[mangSoChan.length - 1];
    }
    if (mangSoChan.length == 0) soChanCuoiCung = -1;
    document.querySelector(".resultEx5").innerHTML = `<p>${soChanCuoiCung}</p>`;
  }
}
function doiCho() {
  var viTri1Value = document.getElementById("txt-number-Ex5-1").value * 1;
  var viTri2Value = document.getElementById("txt-number-Ex5-2").value * 1;
  var tmpValue = numberArray[viTri1Value];
  numberArray[viTri1Value] = numberArray[viTri2Value];
  numberArray[viTri2Value] = tmpValue;
  document.querySelector(
    ".resultEx6"
  ).innerHTML = `<p>Mảng sau khi đổi: ${numberArray}</p>`;
}
function sapXep() {
  for (var i = 0; i < numberArray.length; i++) {
    for (var j = 0; j < numberArray.length - 1; j++) {
      if (numberArray[j] > numberArray[j + 1]) {
        var temp = numberArray[j];
        numberArray[j] = numberArray[j + 1];
        numberArray[j + 1] = temp;
      }
    }
  }
  document.querySelector(
    ".resultEx7"
  ).innerHTML = `<p>Mảng sau khi sắp xếp: ${numberArray}</p>`;
}
function isPrime(n) {
  let flag = 1;
  if (n < 2) return (flag = 0);
  let i = 2;
  while (i < n) {
    if (n % i == 0) {
      flag = 0;
      break;
    }
    i++;
  }
  return flag;
}
function timSoNguyenTo() {
  for (var i = 0; i < numberArray.length; i++) {
    if (isPrime(numberArray[i]) == 1) {
      document.querySelector(
        ".resultEx8"
      ).innerHTML = `<p> Số nguyên tố đầu tiên trong mảng : ${numberArray[i]} </p>`;
      break;
    } else {
      document.querySelector(".resultEx8").innerHTML = `<p> -1 </p>`;
    }
  }
}
function soSanh() {
  countSoDuong = 0;
  for (var i = 0; i < numberArray.length; i++) {
    if (numberArray[i] > 0) {
      countSoDuong++;
    }
    console.log("countSoDuong: ", countSoDuong);
    var countSoAm = numberArray.length - countSoDuong;
    console.log("countSoAm: ", countSoAm);
    var result;
    if (countSoAm == countSoDuong) {
      result = `<p> Số Dương = Số Âm </p>`;
    } else if (countSoAm < countSoDuong) {
      result = `<p> Số Dương > Số Âm </p>`;
    } else {
      result = `<p> Số Dương < Số Âm </p>`;
    }
  }
  document.querySelector(".resultEx10").innerHTML = result;
}

// bài 9
var numberArray2 = [];
function themSo2() {
  var inputNumber = document.querySelector("#txt-number-2");
  if (inputNumber.value == "") {
    return;
  }
  var number = inputNumber.value * 1;
  numberArray2.push(number);

  inputNumber.value = "";
  console.log("numberArray2: ", numberArray2);
  document.getElementById(
    "khaiBaoMang2"
  ).innerHTML = `<p>Mảng hiện tại: ${numberArray2}</p>`;
  return numberArray2;
}
function demSoNguyen() {
  var soLuongSoNguyen = 0;
  for (var i = 0; i < numberArray2.length; i++) {
    if (Number.isInteger(numberArray2[i])) {
      soLuongSoNguyen++;
    }
  }
  document.querySelector(
    ".resultEx9"
  ).innerHTML = `<p>Số lượng số nguyên: ${soLuongSoNguyen}</p>`;
}
